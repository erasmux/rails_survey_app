class SurveysController < ApplicationController
  before_action :set_survey, only: [:show, :edit, :update, :destroy, :answers, :answers_of]

  # GET /surveys
  # GET /surveys.json
  def index
    @surveys = Survey.all
  end

  # GET /surveys/1
  # GET /surveys/1.json
  def show
    @questions = @survey.questions
    @answers = @questions.inject({}) do |res,q|
      q.answers.each do |a|
        (res[a.participant_id] ||= {})[a.question_id] = a
      end
      res
    end
  end

  # GET /surveys/new
  def new
    @survey = Survey.new
  end

  # GET /surveys/1/edit
  def edit
  end

  # POST /surveys
  # POST /surveys.json
  def create
    @survey = Survey.new(survey_params)

    respond_to do |format|
      if @survey.save
        format.html { redirect_to edit_survey_path(@survey), notice: 'Survey was successfully created.' }
        format.json { render :edit, status: :created, location: @survey }
      else
        format.html { render :new }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /surveys/1
  # PATCH/PUT /surveys/1.json
  def update
    respond_to do |format|
      if @survey.update(survey_params)
        format.html { redirect_to edit_survey_path(@survey), notice: 'Survey was successfully updated.' }
        format.json { render :edit, status: :ok, location: @survey }
      else
        format.html { render :edit }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /surveys/1
  # DELETE /surveys/1.json
  def destroy
    @survey.destroy
    respond_to do |format|
      format.html { redirect_to surveys_url, notice: 'Survey was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def answers
  end

  def answers_of
    @participant = params[:participant] && Participant.find(params[:participant])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survey
      @survey = Survey.find(params[:id])
    end

    def survey_params
      survey_params_destroy_blank_answers(survey_params_validate)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def survey_params_validate
      params.require(:survey).permit(
        :name,
        :questions_attributes => [
          :id,
          :content,
          :answers_attributes => [
            :id,
            :content,
            :participant_id
          ]
        ]
      )
    end

    def survey_params_destroy_blank_answers(sparams)
      if sparams && sparams.include?(:questions_attributes)
        sparams[:questions_attributes].each do |qn, qparams|
          if qparams.include?(:answers_attributes)
            qparams[:answers_attributes].each do |an, aparams|
              aparams[:_destroy] = '1' if aparams.include?(:id) && aparams[:content].blank?
            end
          end
        end
      end
      sparams
    end
end
