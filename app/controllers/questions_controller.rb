class QuestionsController < ApplicationController
  before_action :set_survey, only: [:create]
  before_action :set_question, only: [:update, :destroy]

  # POST /surveys/7/questions
  def create
    @question = @survey.questions.create(question_params)

    notice =
      if @question.valid?
        "Question was successfully created."
      else
        "Question creation failed: #{@question.errors.full_messages.join("; ")}"
      end
    redirect_to edit_survey_path(@survey), notice: notice
  end

  # PATCH/PUT /surveys/7/questions/1
  def update
    notice =
      if @question.update(question_params)
        "Question was successfully updated."
      else
        "Question update failed: #{@question.errors.full_messages.join("; ")}"
      end
    redirect_to edit_survey_path(@survey), notice: notice
  end

  # DELETE /surveys/7/questions/1
  def destroy
    notice =
      if @question.destroy && @question.destroyed?
        "Question was successfully destroyed."
      else
        "Question destroy failed: #{@question.errors.full_messages.join("; ")}"
      end
    redirect_to edit_survey_path(@survey), notice: notice
  end

  private
    def set_survey
      @survey = Survey.find(params[:survey_id])
    end

    def set_question
      set_survey
      @question = @survey.questions.find(params[:id])
    end

    def question_params
      params.require(:question).permit(:content)
    end
end
