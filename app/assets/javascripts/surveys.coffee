# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
  $("#participant_select").bind "change", ->
    if $(this).val()
      $('#participant_select option[value=""]').remove()
      $.ajax({
        type: "get",
        url: $(this).attr("post_link"),
        contentType: "application/javascript",
        data: { participant: $(this).val() }
      })
