class Question < ApplicationRecord
  belongs_to :survey
  has_many :answers, dependent: :destroy
  has_many :participants, through: :answers

  accepts_nested_attributes_for :answers,
    allow_destroy: true,
    reject_if: proc { |a| a['content'].blank? }

  validates :content, presence: true, allow_blank: false
end
