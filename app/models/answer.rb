class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :participant

  validates :content, presence: true, allow_blank: false
end
