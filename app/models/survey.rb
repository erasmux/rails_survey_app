class Survey < ApplicationRecord
  has_many :questions, dependent: :destroy

  accepts_nested_attributes_for :questions

  validates :name, presence: true, length: { minimum: 3, maximum: 100 }
end
