class Participant < ApplicationRecord
  has_many :answers, dependent: :destroy
  has_many :questions, through: :answers

  validates :name, presence: true, length: { minimum: 3, maximum: 100 }
end
