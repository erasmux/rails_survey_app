Rails.application.routes.draw do
  resources :surveys do
    resources :questions, :only => [:create, :update, :destroy]
    get 'answers', on: :member
    get 'answers_of', on: :member
  end
  resources :participants
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'surveys#index'
end
